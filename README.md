# Toybrot

## TL;DR:

This is some simple code that generates some fractal and I use it to compare
different parallelization models and backends. It uses SDL2 for display and CMake
on the build, trying to find other dependencies as automagically as possible

## What is ToyBrot?

Toybrot started out initially as a school project for "something that deals
with maths". It was a simple piece of code that calculated a mandelbrto fractal
and drew it to an SDL Surface which then gets drawn on the screen.

Years later, when I was getting to grips with C++11's multithreading support,
I decided to dig this project back up and use it as a demo to get me to implement
some pseudo-real code.

Afterwards I expanded the idea and after some polishing and rework, turned this
project into an experimental ground for my studies of parallelisation in general,
adding different implementations as I got to grips with different programming
models and languages.

Eventually I also decided to start writing a blog series which can be found in
my blog [The Great Refactoring](http://vilelasagna.ddns.net), in a series I called
[Multi Your Threading](http://vilelasagna.ddns.net/tgr/coding/so-you-wanted-to-multi-your-threading-huh/).

As I implement different versions of this code, I write articles on my experiences
with them and compare them to the other implementations, not only in terms of
performance, but also in terms of how difficult I find to actually learn the new
ideas and paradigms and how to put them in code and figure things out when they
don't go quite as expected.

### The Radeon VII incident and the migration to Raymarching

In 2019 I got myself a Radeon VII and it threw somewhat of a wrench in this project
in a curious way. It turns out the Radeon VII is way too powerful for something as
simple as I was doing (the OpenCL implementation averaging about 40ms to generate).

This makes the project very unsuitable for performance comparisons since at that
point any differences will be way too small to be measurable and the set up and
tear down of the subsystems might account for most of the runtime. The answer I
found for this was migrating to a volumetric fractal isntead, using raymarching.
Though there are several implementations designed to run at 60fps on GPUs, it does
provide me the ability to almsot arbitrarily increase the runtime by tweaking the
generation parameters and deliberately choose algorithms such that I can get more
meaningful numbers for comparison


## Building Toybrot

Toybrot uses SDL2 for display and is built with C++11 and forward in mind. These
are the only consistent requirements for building it. Other than that it will try
and find other dependencies and enable projects as it does through CMake.

> The one exception is there is one MPI project which instead of having a gui just
dumps the result straight to disk through `libpng`.

In order to build the legacy Mandelbrot projects, please enable the CMake option
`ENABLE2D`.


## Running Toybrot

Run the program with -h from the CLI to get more information on the available
options, including benchmarking tweaks

## Implementations (which I'll try and keep up to date)

 - C++ STL `std::threads`
 - C++ STL `std::async`
 - OpenMP
 - MPI
 - OpenCL
 - nVidia CUDA
 - AMD HC C++
 - AMD HIP
 - Vulkan
 - SYCL
 - *(Implementations below this will not be ported to the 2d Mandelbrot version)*
  - Intel TBB (*planned*)
  - Intel ISPC (*planned*)
