#ifndef FRACGEN_HPP_DEFINED
#define FRACGEN_HPP_DEFINED

#include <vector>
#include <cstdint>
#include <SDL_pixels.h>
#include "FracGenWindow.hpp"

class FracGen
{
public:

    FracGen(bool benching = false);
    ~FracGen();

    bool Generate(uint32_t* img, SDL_PixelFormat* format, int width, int height, const Camera &c);


private:

    uint32_t getColour();
    bool bench;
};

#endif //FRACGEN_HPP_DEFINED
