#include "FracGen.hpp"

#include <iostream>
#include <cfloat>
#include <hip/hip_runtime.h>
#include <vector>
#include <stdexcept>

/**
  * HIP is very directly based on CUDA but to my knowledge
  * one of the things it lacks in comparison is an equivalent
  * of nvstd::function. Even though I elected not to use it on
  * the CUDA version, I'm leaving it here as a reminder that
  * it's not EXACTLY 1:1. It's overwhelmingly the same though
  * and whichever you write first, converting to the other is
  * usually trivial
  */
//using  estimatorFunction = nvstd::function<float(Vec3f)>;



/******************************************************************************
 *
 * HIP helper functions
 *
 ******************************************************************************/

/**
 * To me the fact that we're back to this type of error checking instead of
 * having actual exceptions is one of the weaknesses of HIP that come from it
 * being so tied to CUDA
 */

inline void hipCheck(int line)
{
    auto status = hipGetLastError();
    if(status != hipError_t::hipSuccess)
    {
        std::cout << "HIP error @" << line <<" -> " << hipGetErrorString(status) << std::endl;
        throw std::runtime_error(hipGetErrorString(status));
    }
}

//specialize to avoid std::sqrt
template<>
__host__ __device__ float Vec3<float>::mod()
{
    return sqrtf(sqMod());
}


/******************************************************************************
 *
 * Tweakable parameters
 *
 ******************************************************************************/


__device__ static const size_t  maxRaySteps      = 7500;
__device__ static const float   collisionMinDist = 0.00055f;

// coulouring parameters

__device__ static const float           hueFactor   = -40.0f;
__device__ static const int             hueOffset   = 245;
__device__ static const float           valueFactor = 42;
__device__ static const float           valueClamp  = 0.9f;
__device__ static const float           satValue    = 0.7f;
__device__ static const unsigned char   bgValue     = 40;

// Mandelbox constants
__device__ static const float   fixedRadiusSq   = 2.2f;
__device__ static const float   minRadiusSq     = 0.8f;
__device__ static const float   foldingLimit    = 1.45f;
__device__ static const float   boxScale        = -3.5;
__device__ static const size_t  boxIterations   = 30;



/******************************************************************************
 *
 * Distance estimator functions and helpers
 *
 ******************************************************************************/



__device__ inline void sphereFold(Vec3f& z, float& dz)
{
    float r2 = z.sqMod();

    /**
      * Traditional GPU coding wisdom dictates you should avoid
      * having branching instructions, such as "if" statements.
      * One way to avoid it is turning them into conditional
      * mathematical expressions, such as the ones under here
      * However, once I tried doing this I actually had massive
      * slowdown compared to the more straighforward if blocks.
      *
      * I left this here to possibly revisit in the future and
      * as a curiosity. (Or heck, maybe I just did it very wrong)
      */

//    int cond1 = r2 < minRadiusSq;
//    int cond2 = (r2 < fixedRadiusSq) * !cond1;
//    //int cond3 = !cond1 * !cond2;
//    //float temp = ( (fixedRadiusSq/minRadiusSq) * cond1) + ( (fixedRadiusSq/r2) * cond2) + cond3;

//    float temp[]{1,fixedRadiusSq/minRadiusSq , fixedRadiusSq/r2};
//    int tempidx = cond1 + 2*cond2;
//    z *= temp[tempidx];
//    dz *= temp[tempidx];

    if ( r2 < minRadiusSq)
    {
        // linear inner scaling
        float temp = (fixedRadiusSq/minRadiusSq);
        z *= temp;
        dz *= temp;
    }
    else if(r2<fixedRadiusSq)
    {
        // this is the actual sphere inversion
        float temp =(fixedRadiusSq/r2);
        z *= temp;
        dz*= temp;
    }
}

__device__ inline void boxFold(Vec3f& z)
{
    z = z.clamp(-foldingLimit, foldingLimit)* 2.0f - z;
}

__device__ float boxDist(const Vec3f& p)
{
    /**
     * Distance estimator for a mandelbox
     *
     * Distance estimator adapted from
     * https://http://blog.hvidtfeldts.net/index.php/2011/11/distance-estimated-3d-fractals-vi-the-mandelbox/
     */
    const Vec3f& offset = p;
    float dr = boxScale;
    Vec3f z = p;
    for (size_t n = 0; n < boxIterations; n++)
    {
        boxFold(z);       // Reflect
        sphereFold(z,dr);    // Sphere Inversion

        z = z * boxScale + offset;  // Scale & Translate
        dr = dr * abs(boxScale) + 1.0f;

    }

    return z.mod()/abs(dr);
}


__device__ float bulbDist(const Vec3f& p)
{

    /**
     * Distance estimator for a mandelbulb
     *
     * Distance estimator adapted from
     * https://www.iquilezles.org/www/articles/mandelbulb/mandelbulb.htm
     * https://www.shadertoy.com/view/ltfSWn
     */

    Vec3f w = p;
    float m = w.sqMod();

    //vec4 trap = vec4(abs(w),m);
    float dz = 3.0f;


    for( int i=0; i<4; i++ )
    {
#if 1
        float m2 = m*m;
        float m4 = m2*m2;
        dz = 8.0f*sqrtf(m4*m2*m)*dz + 1.0f;

        float x = w.X(); float x2 = x*x; float x4 = x2*x2;
        float y = w.Y(); float y2 = y*y; float y4 = y2*y2;
        float z = w.Z(); float z2 = z*z; float z4 = z2*z2;

        float k3 = x2 + z2;
        float k2 = 1/sqrtf( k3*k3*k3*k3*k3*k3*k3 );
        float k1 = x4 + y4 + z4 - 6.0f*y2*z2 - 6.0f*x2*y2 + 2.0f*z2*x2;
        float k4 = x2 - y2 + z2;

        w.setX(p.X() +  64.0f*x*y*z*(x2-z2)*k4*(x4-6.0f*x2*z2+z4)*k1*k2);
        w.setY(p.Y() + -16.0f*y2*k3*k4*k4 + k1*k1);
        w.setZ(p.Z() +  -8.0f*y*k4*(x4*x4 - 28.0f*x4*x2*z2 + 70.0f*x4*z4 - 28.0f*x2*z2*z4 + z4*z4)*k1*k2);
#else
        dz = 8.0*pow(sqrt(m),7.0)*dz + 1.0;
        //dz = 8.0*pow(m,3.5)*dz + 1.0;

        float r = w.mod();
        float b = 8.0*acos( w.Y()/r);
        float a = 8.0*atan2( w.X(), w.Z() );
        w = p + Vec3f( sin(b)*sin(a), cos(b), sin(b)*cos(a) ) * pow(r,8.0);
#endif

       // trap = min( trap, vec4(abs(w),m) );

        m = w.sqMod();
        if( m > 256.0f )
            break;
    }

    return 0.25f*log(m)*sqrtf(m)/dz;
}

__device__ float sphereDist(Vec3f p)
{
    float radius = 2.f;
    return p.mod() - radius;
}

/******************************************************************************
 *
 * Coulouring functions and helpers
 *
 ******************************************************************************/

__device__ RGBA HSVtoRGB(int H, float S, float V)
{

    /**
     * adapted from
     * https://gist.github.com/kuathadianto/200148f53616cbd226d993b400214a7f
     */

    RGBA output;
    float C = S * V;
    float X = C * (1 - abs(fmodf(H / 60.0, 2) - 1));
    float m = V - C;
    float Rs, Gs, Bs;

    if(H >= 0 && H < 60)
    {
        Rs = C;
        Gs = X;
        Bs = 0;
    }
    else if(H >= 60 && H < 120)
    {
        Rs = X;
        Gs = C;
        Bs = 0;
    }
    else if(H >= 120 && H < 180)
    {
        Rs = 0;
        Gs = C;
        Bs = X;
    }
    else if(H >= 180 && H < 240)
    {
        Rs = 0;
        Gs = X;
        Bs = C;
    }
    else if(H >= 240 && H < 300)
    {
        Rs = X;
        Gs = 0;
        Bs = C;
    }
    else {
        Rs = C;
        Gs = 0;
        Bs = X;
    }

    output.r = (Rs + m) * 255 ;
    output.g = (Gs + m) * 255 ;
    output.b = (Bs + m) * 255 ;

    return output;
}

__device__ RGBA getColour(const Vec4f& steps)
{
    RGBA colour;

    Vec3f position(steps.X(),steps.Y(),steps.Z());

    RGBA background;
    background.r = background.g = background.b = 40;

    if(steps.W() == maxRaySteps)
    {
        return background;
    }



    float saturation = 0.8f;
    int hue = (static_cast<int>(position.Z() * hueFactor) + hueOffset) % 360;
    float value = 1.0f - min(steps.W()*valueFactor/maxRaySteps, valueClamp);

    colour = HSVtoRGB(hue, saturation, value);

    //    Simplest colouring, based only on steps (roughly distance from camera)

    //    colour.r = static_cast<uint8_t>(255*value);
    //    colour.g = static_cast<uint8_t>(255*value);
    //    colour.b = static_cast<uint8_t>(255*value);

    return colour;

}

__device__ uint32_t MapSDLRGBA(RGBA colour, SDL_PixelFormat format)
{
    return    ( colour.r >> format.Rloss) << format.Rshift
            | ( colour.g >> format.Gloss) << format.Gshift
            | ( colour.b >> format.Bloss) << format.Bshift
            | ((colour.a >> format.Aloss) << format.Ashift & format.Amask  );
}

/******************************************************************************
 *
 * Ray marching functions and helpers
 *
 ******************************************************************************/

__device__ Vec4f trace( const Camera& cam,  const Screen& s, int x, int y/*, const estimatorFunction& f*/)
{
    /**
     * This function taken from
     * http://blog.hvidtfeldts.net/index.php/2011/06/distance-estimated-3d-fractals-part-i/
     */

    float totalDistance = 0.0f;
    unsigned int steps;

    Vec3f pixelPosition = s.topLeft + Vec3f{s.pixelWidth*x, s.pixelHeight * y, 0.f};

    Vec3f rayDir = pixelPosition - static_cast<Vec3f>(cam.pos);
    rayDir.normalise();

    Vec3f p;
    for (steps=0; steps < maxRaySteps; steps++)
    {
        p = cam.pos + (rayDir * totalDistance);
        //float distance = f(p);
        float distance = boxDist(p);
        totalDistance += distance;
        if (distance < collisionMinDist) break;
    }

    //return both the steps and the actual position in space for colouring purposes
    return Vec4f{p,static_cast<float>(steps)};
}

__global__ void traceRegion(uint32_t* data,
                Camera cam, Screen scr,
                SDL_PixelFormat format)
{

    int col = blockIdx.x * blockDim.x + threadIdx.x;
    int row = blockIdx.y * blockDim.y + threadIdx.y;

    int index = ((row*scr.width)+col);
    if (col >= scr.width || row >= scr.height)
    {
        return;
    }

    data[index] = MapSDLRGBA( getColour( trace(cam, scr, col, row) ) , format);
}

/******************************************************************************
 *
 * Thread spawning section
 *
 ******************************************************************************/

bool FracGen::Generate(uint32_t* img, SDL_PixelFormat* format, int width, int height, const Camera& c)
{
    if(format == nullptr)
    {
        return false;
    }

    /*
     * calculate the rectangle which represents the screen (camera z near) in object space
     * No need to have an actual general camera so I'm just assuming the camera
     * always sits on the Z axis and always has (0,1,0) as it's up vector

     * This allows me to cheat a lot and not have to actually go into the
     * linear algebra side and write something like gluUnproject
     */
    Screen s;

    Vec3f screenPlaneOrigin{c.pos.X(),c.pos.Y(),c.pos.Z() + c.near};
    float screenPlaneHeight = 2*(c.near*sin(c.fovY/2));
    screenPlaneHeight = screenPlaneHeight < 0 ? -screenPlaneHeight : screenPlaneHeight;
    float screenPlaneWidth = screenPlaneHeight * c.AR;
    // if 0,0 is top left, pixel height needs to be a negative
    s.width  = width;
    s.height = height;
    s.pixelHeight = (-1.f) * screenPlaneHeight / s.height;
    s.pixelWidth = screenPlaneWidth / s.width;
    s.topLeft = Vec3f {screenPlaneOrigin.X() - (screenPlaneWidth/2),
                       screenPlaneOrigin.Y() + (screenPlaneHeight/2),
                       screenPlaneOrigin.Z()                          };

    dim3 threadsPerBlock(16,16);
    dim3 numBlocks(std::ceil(static_cast<float>(width)/threadsPerBlock.x), std::ceil(static_cast<float>(height)/threadsPerBlock.y));
    uint32_t* devVect;
    hipMalloc(&devVect, width*height*sizeof(uint32_t));
    hipLaunchKernelGGL(traceRegion, numBlocks,threadsPerBlock, 0, 0,devVect, c, s, *format);
    hipCheck(__LINE__);
    hipDeviceSynchronize();
    hipCheck(__LINE__);
    hipMemcpy(img, devVect, width*height*sizeof(uint32_t), hipMemcpyDeviceToHost);
    hipCheck(__LINE__);
    hipFree(devVect);

    return true;
}

FracGen::FracGen(bool benching)
    :bench{benching}
{
    static bool once = false;

    hipDeviceProp_t prop;
    hipGetDeviceProperties(&prop, 0);
    hipCheck(__LINE__);
    if(!once || !benching )
    {
        once = true;
        std::cout << "Running hip on:        "   << prop.name                      << std::endl;
        std::cout << "Clocked at:            "   << prop.clockRate/1000            << "MHz" << std::endl;
        std::cout << "Max Mem:               "   << prop.totalGlobalMem/1000000000 << "GB" << std::endl;
        std::cout << "Max threads per block: "   << prop.maxThreadsPerBlock        << std::endl;
    }
}

FracGen::~FracGen()
{
    hipDeviceReset();
    hipCheck(__LINE__);
}



