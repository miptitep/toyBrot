#include "FracGen.hpp"

#include <iostream>
#include <cfloat>
#include <functional>
#include <atomic>
#include <vector>

#include <omp.h>

//static Vec3f boxMaxes;
//static Vec3f boxMins;


using  estimatorFunction = std::function<float(Vec3f)>;


/******************************************************************************
 *
 * Tweakable parameters
 *
 ******************************************************************************/

static constexpr const size_t maxRaySteps     = 7500;
static constexpr const float collisionMinDist = 0.00055f;

// coulouring parameters

static constexpr const float hueFactor   = -60.0f;
static constexpr const int   hueOffset   = 365;
static constexpr const float valueFactor = 32;
static constexpr const float valueClamp  = 0.9f;
static constexpr const float satValue    = 0.5f;
static constexpr const uint8_t bgValue   = 40;


// Mandelbox constants
static constexpr const float fixedRadiusSq  = 2.2f;
static constexpr const float minRadiusSq    = 0.8f;
static constexpr const float foldingLimit   = 1.45;
static constexpr const float boxScale       = -3.5f;
static constexpr const size_t boxIterations = 30;


/******************************************************************************
 *
 * Distance estimator functions and helpers
 *
 ******************************************************************************/



void sphereFold(Vec3f& z, float& dz)
{
    float r2 = z.sqMod();
    if ( r2 < minRadiusSq)
    {
        // linear inner scaling
        float temp = (fixedRadiusSq/minRadiusSq);
        z *= temp;
        dz *= temp;
    }
    else if(r2<fixedRadiusSq)
    {
        // this is the actual sphere inversion
        float temp =(fixedRadiusSq/r2);
        z *= temp;
        dz*= temp;
    }
}

void boxFold(Vec3f& z)
{
    z = z.clamp(-foldingLimit, foldingLimit)* 2.0f - z;
}

float boxDist(const Vec3f& p)
{
    /**
     * Distance estimator for a mandelbox
     *
     * Distance estimator adapted from
     * https://http://blog.hvidtfeldts.net/index.php/2011/11/distance-estimated-3d-fractals-vi-the-mandelbox/
     */
    const Vec3f& offset = p;
    float dr = boxScale;
    Vec3f z{p};
    for (size_t n = 0; n < boxIterations; n++)
    {
        boxFold(z);       // Reflect
        sphereFold(z,dr);    // Sphere Inversion

        z = z * boxScale + offset;  // Scale & Translate
        dr = dr * abs(boxScale) + 1.0f;


    }
    float r = z.mod();
    return r/std::abs(dr);
}


float bulbDist(const Vec3f& p)
{

    /**
     * Distance estimator for a mandelbulb
     *
     * Distance estimator adapted from
     * https://www.iquilezles.org/www/articles/mandelbulb/mandelbulb.htm
     * https://www.shadertoy.com/view/ltfSWn
     */

    Vec3f w = p;
    float m = w.sqMod();

    //vec4 trap = vec4(abs(w),m);
    float dz = 3.0f;


    for( int i=0; i<4; i++ )
    {
#if 1
        float m2 = m*m;
        float m4 = m2*m2;
        dz = 8.0f*sqrt(m4*m2*m)*dz + 1.0f;

        float x = w.X(); float x2 = x*x; float x4 = x2*x2;
        float y = w.Y(); float y2 = y*y; float y4 = y2*y2;
        float z = w.Z(); float z2 = z*z; float z4 = z2*z2;

        float k3 = x2 + z2;
        float k2 = 1/sqrt( k3*k3*k3*k3*k3*k3*k3 );
        float k1 = x4 + y4 + z4 - 6.0f*y2*z2 - 6.0f*x2*y2 + 2.0f*z2*x2;
        float k4 = x2 - y2 + z2;

        w.setX(p.X() +  64.0f*x*y*z*(x2-z2)*k4*(x4-6.0f*x2*z2+z4)*k1*k2);
        w.setY(p.Y() + -16.0f*y2*k3*k4*k4 + k1*k1);
        w.setZ(p.Z() +  -8.0f*y*k4*(x4*x4 - 28.0f*x4*x2*z2 + 70.0f*x4*z4 - 28.0f*x2*z2*z4 + z4*z4)*k1*k2);
#else
        dz = 8.0*pow(sqrt(m),7.0)*dz + 1.0;
        //dz = 8.0*pow(m,3.5)*dz + 1.0;

        float r = w.mod();
        float b = 8.0*acos( w.Y()/r);
        float a = 8.0*atan2( w.X(), w.Z() );
        w = p + Vec3f( sin(b)*sin(a), cos(b), sin(b)*cos(a) ) * pow(r,8.0);
#endif

       // trap = min( trap, vec4(abs(w),m) );

        m = w.sqMod();
        if( m > 256.0f )
            break;
    }

    return 0.25f*log(m)*sqrt(m)/dz;
}

float sphereDist(Vec3f p)
{
    float radius = 2.f;
    return p.mod() - radius;
}

/******************************************************************************
 *
 * Coulouring functions and helpers
 *
 ******************************************************************************/

RGBA HSVtoRGB(int H, float S, float V)
{

    /**
     * adapted from
     * https://gist.github.com/kuathadianto/200148f53616cbd226d993b400214a7f
     */

    RGBA output;
    float C = S * V;
    float X = C * (1 - std::abs(std::fmod(H / 60.0, 2) - 1));
    float m = V - C;
    float Rs, Gs, Bs;

    if(H >= 0 && H < 60)
    {
        Rs = C;
        Gs = X;
        Bs = 0;
    }
    else if(H >= 60 && H < 120)
    {
        Rs = X;
        Gs = C;
        Bs = 0;
    }
    else if(H >= 120 && H < 180)
    {
        Rs = 0;
        Gs = C;
        Bs = X;
    }
    else if(H >= 180 && H < 240)
    {
        Rs = 0;
        Gs = X;
        Bs = C;
    }
    else if(H >= 240 && H < 300)
    {
        Rs = X;
        Gs = 0;
        Bs = C;
    }
    else {
        Rs = C;
        Gs = 0;
        Bs = X;
    }

    output.r = (Rs + m) * 255 ;
    output.g = (Gs + m) * 255 ;
    output.b = (Bs + m) * 255 ;

    return output;
}

RGBA getColour(const Vec4f& steps)
{
    RGBA colour;

    Vec3f position(steps.X(),steps.Y(),steps.Z());

    RGBA background;
    background.r = background.g = background.b = bgValue;

    if(steps.W() == maxRaySteps)
    {
        return background;
    }

//        This is a good place to check your bounds if you need to

//        boxMins.setX(std::min(boxMins.X(),position.X()));
//        boxMins.setY(std::min(boxMins.Y(),position.Y()));
//        boxMins.setZ(std::min(boxMins.Z(),position.Z()));

//        boxMaxes.setX(std::max(boxMaxes.X(),position.X()));
//        boxMaxes.setY(std::max(boxMaxes.Y(),position.Y()));
//        boxMaxes.setZ(std::max(boxMaxes.Z(),position.Z()));


    float saturation = satValue;
    int hue = (static_cast<int>(position.Z() * hueFactor) + hueOffset) % 360;
    float value = 1.0f - std::min(steps.W()*valueFactor/maxRaySteps, valueClamp);

    colour = HSVtoRGB(hue, saturation, value);


//    Simplest colouring, based only on steps (roughly distance from camera)

//    colour.r = static_cast<uint8_t>(255*value);
//    colour.g = static_cast<uint8_t>(255*value);
//    colour.b = static_cast<uint8_t>(255*value);

    return colour;

}

uint32_t MapSDLRGBA(RGBA colour,  const SDL_PixelFormat& format)
{
    return    ( colour.r >> format.Rloss) << format.Rshift
            | ( colour.g >> format.Gloss) << format.Gshift
            | ( colour.b >> format.Bloss) << format.Bshift
            | ((colour.a >> format.Aloss) << format.Ashift & format.Amask  );
}

/******************************************************************************
 *
 * Ray marching functions and helpers
 *
 ******************************************************************************/

Vec4f trace(const Camera& cam, const Screen& s, int x, int y, estimatorFunction& f)
{
    /**
     * This function taken from
     * http://blog.hvidtfeldts.net/index.php/2011/06/distance-estimated-3d-fractals-part-i/
     */

    float totalDistance = 0.0f;
    unsigned int steps;

    Vec3f pixelPosition = s.topLeft + Vec3f{s.pixelWidth*x, s.pixelHeight * y, 0.f};

    Vec3f rayDir = pixelPosition -  static_cast<Vec3f>(cam.pos);
    rayDir.normalise();

    Vec3f p;
    for (steps=0; steps < maxRaySteps; steps++)
    {
        p = cam.pos + (rayDir * totalDistance);
        float distance = f(p);
        totalDistance += distance;
        if (distance < collisionMinDist) break;
    }
    //return both the steps and the actual position in space for colouring purposes
    return Vec4f{p,static_cast<float>(steps)};
}

bool traceRegion(uint32_t* data,
                const Camera& cam, const Screen& scr,
                estimatorFunction& f,
                const SDL_PixelFormat& format,
                int h0, int heightStep)
{
    for(int h = h0; h < h0+heightStep; h++)
    {
        if (h >= scr.height)
        {
            return true;
        }

        for(int w = 0 + omp_get_thread_num(); w < scr.width; w+= omp_get_num_threads() )
        {
            data[(h*scr.width)+w] = MapSDLRGBA(getColour(trace(cam, scr, w, h, f)), format);
        }
    }
    return false;
}

/******************************************************************************
 *
 * Thread spawning section
 *
 ******************************************************************************/

bool FracGen::Generate(uint32_t* img, SDL_PixelFormat* format, int width, int height, const Camera& c)
{
    if(format == nullptr)
    {
        return false;
    }
    static std::atomic<int> h {0};
    bool finishedGeneration = false;
    int heightStep = bench ? height : 10;

    estimatorFunction bulb(bulbDist);
    estimatorFunction box(boxDist);

    /*
     * calculate the rectangle which represents the screen (camera z near) in object space
     * No need to have an actual general camera so I'm just assuming the camera
     * always sits on the Z axis and always has (0,1,0) as it's up vector

     * This allows me to cheat a lot and not have to actually go into the
     * linear algebra side and write something like gluUnproject
     */
    Screen s;

    Vec3f screenPlaneOrigin{c.pos.X(),c.pos.Y(),c.pos.Z() + c.near};
    float screenPlaneHeight = 2*(c.near*sin(c.fovY/2));
    screenPlaneHeight = screenPlaneHeight < 0 ? -screenPlaneHeight : screenPlaneHeight;
    float screenPlaneWidth = screenPlaneHeight * c.AR;
    // if 0,0 is top left, pixel height needs to be a negative
    s.width  = width;
    s.height = height;
    s.pixelHeight = (-1.f) * screenPlaneHeight / s.height;
    s.pixelWidth = screenPlaneWidth / s.width;
    s.topLeft = Vec3f {screenPlaneOrigin.X() - (screenPlaneWidth/2),
                       screenPlaneOrigin.Y() + (screenPlaneHeight/2),
                       screenPlaneOrigin.Z()                          };

    std::vector<bool> results(static_cast<size_t>(omp_get_num_procs()));

    #pragma omp parallel for
    for(size_t i = 0; i < static_cast<size_t>(omp_get_num_procs()); i++)
    {
        results[i]= traceRegion(img, c, s, box, *format, h.load(), heightStep);
    }

    h+= heightStep;

    for(bool b: results)
    {
        if(b)
        {
            //If one of them is done, they all must be
            h.store(0);
            finishedGeneration = true;
//            std::cout << "Minimum bounds = " << boxMins.X() << " " << boxMins.Y() << " " << boxMins.Z() << std::endl;
//            std::cout << "Maximum bounds = " << boxMaxes.X() << " " << boxMaxes.Y() << " " << boxMaxes.Z() << std::endl;
        }
    }
    return finishedGeneration;
}

FracGen::FracGen(bool benching)
    :bench{benching}
{
    static bool once = false;
    if(!bench || !once)
    {
      std::cout << " OpenMP reports " << omp_get_num_procs() << " native threads" << std::endl;
      once = true;
    }
}

FracGen::~FracGen()
{}



