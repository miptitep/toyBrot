/******************************************************************************
 *
 * Tweakable parameters
 *
 ******************************************************************************/


__constant ulong maxRaySteps      = 7500;
__constant float collisionMinDist = 0.00055f;

// coulouring parameters

__constant float hueFactor   = -60.0f;
__constant int hueOffset     = 260;
__constant float valueFactor = 32;
__constant float valueClamp  = 0.8f;
__constant float satValue    = 0.8f;
__constant uchar bgValue     = 40;


// Mandelbox constants
__constant float fixedRadiusSq =  2.2f;
__constant float minRadiusSq   =  0.8f;
__constant float foldingLimit  =  1.45f;
__constant float boxScale      = -3.5f;
__constant ulong boxIterations =  30;


/******************************************************************************
 *
 * Data structs
 *
 ******************************************************************************/


struct __attribute__ ((packed)) Camera
{
    float3 pos;
    float3 up;
    float3 target;
    float AR;
    float near;
    float fovY;
};

struct __attribute__ ((packed)) Screen
{
    float3 topLeft;
    int width;
    int height;
    float pixelWidth;
    float pixelHeight;
};


struct __attribute__ ((packed)) _sdl_pf_cl
{
    uint Amask;
    uchar Rloss;
    uchar Gloss;
    uchar Bloss;
    uchar Aloss;
    uchar Rshift;
    uchar Gshift;
    uchar Bshift;
    uchar Ashift;
};




/******************************************************************************
 *
 * Distance estimator functions
 *
 ******************************************************************************/


void sphereFold(float3* z, float* dz)
{

    float rsq = dot(*z,*z);

    /**
      * Traditional GPU coding wisdom dictates you should avoid
      * having branching instructions, such as "if" statements.
      * One way to avoid it is turning them into conditional
      * mathematical expressions, such as the ones under here
      * However, once I tried doing this I actually had massive
      * slowdown compared to the more straighforward if blocks.
      *
      * I left this here to possibly revisit in the future and
      * as a curiosity. (Or heck, maybe I just did it very wrong)
      */
//    int cond1 = rsq < minRadiusSq;
//    int cond2 = (rsq < fixedRadiusSq) * !cond1;
//    int cond3 = !cond1 * !cond2;
//    float temp = ( (fixedRadiusSq/minRadiusSq) * cond1) + ( (fixedRadiusSq/rsq) * cond2) + cond3;
//    *z *= temp;
//    *dz *= temp;
    if ( rsq < minRadiusSq)
    {
        // linear inner scaling
        float temp = (fixedRadiusSq/minRadiusSq);
        *z *= temp;
        *dz *= temp;
    }
    else if(rsq < fixedRadiusSq )
    {
        // this is the actual sphere inversion
        float temp =(fixedRadiusSq/rsq);
        *z *= temp;
        *dz *= temp;
    }
}

void boxFold(float3* z)
{
    *z = clamp(*z, -foldingLimit, foldingLimit)* 2.0f - *z;
}

float boxDist(float3 p)
{
    /**
     * Distance estimator for a mandelbox
     *
     * Distance estimator adapted from
     * https://http://blog.hvidtfeldts.net/index.php/2011/11/distance-estimated-3d-fractals-vi-the-mandelbox/
     */
    float3 offset = p;
    float dr = boxScale;
    float3 z = p;
    for (size_t n = 0; n < boxIterations; n++)
    {
        boxFold(&z);       // Reflect
        sphereFold(&z,&dr);    // Sphere Inversion

        z = z * boxScale + offset;  // Scale & Translate
        dr = dr * fabs(boxScale) + 1.0f;


    }
    float r = length(z);
    return r/fabs(dr);
}


/******************************************************************************
 *
 * Colouring functions
 *
 ******************************************************************************/



uchar4 HSVtoRGB(int H, float S, float V)
{

    /**
     * adapted from
     * https://gist.github.com/kuathadianto/200148f53616cbd226d993b400214a7f
     */

    uchar4 output;
    float C = S * V;
    float X = C * (1 - fabs(fmod(H / 60.0f, 2) - 1));
    float m = V - C;
    float Rs, Gs, Bs;

    if(H >= 0 && H < 60)
    {
        Rs = C;
        Gs = X;
        Bs = 0;
    }
    else if(H >= 60 && H < 120)
    {
        Rs = X;
        Gs = C;
        Bs = 0;
    }
    else if(H >= 120 && H < 180)
    {
        Rs = 0;
        Gs = C;
        Bs = X;
    }
    else if(H >= 180 && H < 240)
    {
        Rs = 0;
        Gs = X;
        Bs = C;
    }
    else if(H >= 240 && H < 300)
    {
        Rs = X;
        Gs = 0;
        Bs = C;
    }
    else {
        Rs = C;
        Gs = 0;
        Bs = X;
    }

    output.x = (Rs + m) * 255 ;
    output.y = (Gs + m) * 255 ;
    output.z = (Bs + m) * 255 ;
    output.w = 255;

    return output;
}

uchar4 getColour(float4 steps)
{
    uchar4 colour;

    float3 position = (float3)(steps.x,steps.y,steps.z);

    uchar4 background;
    background.x = background.y = background.z = bgValue;

    if(steps.w == maxRaySteps)
    {
        return background;
    }
    else
    {

        float saturation = satValue;
        int hue = ((int) (position.z * hueFactor) + hueOffset) % 360;
        float value = 1.0 - min( (float)( (steps.w*valueFactor) /maxRaySteps), valueClamp);

        colour = HSVtoRGB(hue, saturation, value);

//        Simplest colouring, based only on steps (roughly distance from camera)

//        colour.r = static_cast<uint8_t>(255*value);
//        colour.g = static_cast<uint8_t>(255*value);
//        colour.b = static_cast<uint8_t>(255*value);

        return colour;
    }
}


uint MapSDLRGBA(uchar4 colour,  struct _sdl_pf_cl format)
{
    return  (   colour.s0 >> format.Rloss) << format.Rshift
            | ( colour.s1 >> format.Gloss) << format.Gshift
            | ( colour.s2 >> format.Bloss) << format.Bshift
            | ((colour.s3 >> format.Aloss) << format.Ashift & format.Amask  );
}


/******************************************************************************
 *
 * Ray marching functions and entry kernel
 *
 ******************************************************************************/

float4 trace(struct Camera cam, struct Screen s, int x, int y)
{
    /**
     * This function taken from
     * http://blog.hvidtfeldts.net/index.php/2011/06/distance-estimated-3d-fractals-part-i/
     */

    float totalDistance = 0.0;
    unsigned int steps;

    float3 pixelPosition = s.topLeft + (float3)(s.pixelWidth*x, s.pixelHeight * y, 0);

    float3 rayDir = pixelPosition - cam.pos;
    rayDir = normalize(rayDir);

    float3 p;
    for (steps=0; steps < maxRaySteps; steps++)
    {
        p = cam.pos + (rayDir * totalDistance);
        float distance = boxDist(p);
        totalDistance += distance;
        if (distance < collisionMinDist) break;
    }
    //return both the steps and the actual position in space for colouring purposes
    return (float4)(p, steps);
}


kernel void traceRegion( __global uint* data,
                                int width,
                                int height,
                                struct Camera cam,
                                struct Screen scr,
                                struct _sdl_pf_cl format)
{
    int row = get_global_id (1);
    int col = get_global_id (0);
    int index = ((row*width)+col);

    if (index > width*height)
    {
        return;
    }

    //Not doing the functional shenanigans from STL here
    data[index] = MapSDLRGBA(getColour(trace(cam, scr, col, row)), format);

}
