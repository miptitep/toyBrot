#include "FracGen.hpp"

#include <iostream>
#include <fstream>
#include <cfloat>
#include <string>

/******************************************************************************
 *
 * OpenCL device side data structs
 *
 ******************************************************************************/

using sdl_pf_cl = struct __attribute__ ((packed)) _sdl_pf_cl
{
    _sdl_pf_cl(const SDL_PixelFormat& format)
        : Amask{format.Amask}
        , Rloss{format.Rloss}
        , Gloss{format.Gloss}
        , Bloss{format.Bloss}
        , Aloss{format.Aloss}
        , Rshift{format.Rshift}
        , Gshift{format.Gshift}
        , Bshift{format.Bshift}
        , Ashift{format.Ashift}
    {}

    cl_uint Amask;
    cl_uchar Rloss;
    cl_uchar Gloss;
    cl_uchar Bloss;
    cl_uchar Aloss;
    cl_uchar Rshift;
    cl_uchar Gshift;
    cl_uchar Bshift;
    cl_uchar Ashift;
};

using cl_cam = struct __attribute__ ((packed)) _cl_cam
{
    _cl_cam(const Camera& c)
        : pos{{c.pos.X(), c.pos.Y(), c.pos.Z()}}
        , up{{c.up.X(), c.up.Y(), c.up.Z()}}
        , target{{c.target.X(), c.target.Y(), c.target.Z()}}
        , AR{c.AR}
        , near{c.near}
        , fovY{c.fovY}
    {}

    cl_float3 pos;
    cl_float3 up;
    cl_float3 target;
    cl_float AR;
    cl_float near;
    cl_float fovY;
};

using cl_screen = struct __attribute__ ((packed)) _cl_screen
{
    _cl_screen(const Screen& s)
        : topleft{{s.topLeft.X(), s.topLeft.Y(),s.topLeft.Z()}}
        , width{s.width}
        , height{s.height}
        , pixelWidth{s.pixelWidth}
        , pixelHeight{s.pixelHeight}
    {}

    cl_float3 topleft;
    cl_int width;
    cl_int height;
    cl_float pixelWidth;
    cl_float pixelHeight;
};

/******************************************************************************
 *
 * OpenCL setup and kernel call
 *
 ******************************************************************************/

void FracGen::compileOpenCLKernel()
{
    std::ifstream srcFile("FracGen.cl");
    std::vector<std::string> lines;
    std::string ln;
    lines.push_back(ln);
    if(srcFile.is_open())
    {
        while(std::getline(srcFile,ln))
        {
            ln.append("\n");
            lines.push_back(ln);
        }
    }
    else
    {
        std::cerr << "Could not open OpenCL source file" << std::endl;
        exit(1);
    }
    srcFile.close();
    clProgram = cl::Program(context,lines);

    try
    {
        clProgram.build(devices);
    }
    catch (cl::Error e)
    {
        std::cerr
        << "OpenCL compilation error" << std::endl
        << clProgram.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices[0])
        << std::endl;
        exit(1);
    }

    try
    {
        clGen = cl::Kernel(clProgram, "traceRegion");
    }
    catch (cl::Error err)
    {
        std::cerr
            << "OpenCL kernel creation error: "
            << err.what() << "(" << err.err() << ")"
            << std::endl;
        return;
    }
}


void FracGen::Generate(uint32_t* img, SDL_PixelFormat* format, int width, int height, const Camera& c)
{
    if(format == nullptr)
    {
        return;
    }

    size_t bufferSize = sizeof(uint32_t) * static_cast<size_t>(width) * static_cast<size_t>(height);

    sdl_pf_cl fmt(*format);

    Screen s;

    Vec3f screenPlaneOrigin{c.pos.X(),c.pos.Y(),c.pos.Z() + c.near};
    float screenPlaneHeight = 2*(c.near*sin(c.fovY/2));
    screenPlaneHeight = screenPlaneHeight < 0 ? -screenPlaneHeight : screenPlaneHeight;
    float screenPlaneWidth = screenPlaneHeight * c.AR;
    // if 0,0 is top left, pixel height needs to be a negative
    s.width  = width;
    s.height = height;
    s.pixelHeight = (-1.) * screenPlaneHeight / s.height;
    s.pixelWidth = screenPlaneWidth / s.width;
    s.topLeft = Vec3f {screenPlaneOrigin.X() - (screenPlaneWidth/2),
                       screenPlaneOrigin.Y() + (screenPlaneHeight/2),
                       screenPlaneOrigin.Z()                            };

    try
    {
        const cl::NDRange dims(static_cast<size_t>(width), static_cast<size_t>(height));
        const cl::NDRange offset(0,0);

        cl::Buffer v_dev(context,CL_MEM_WRITE_ONLY, bufferSize);

        clGen.setArg(0, v_dev);
        clGen.setArg(1, width);
        clGen.setArg(2, height);
        clGen.setArg(3, cl_cam(c));
        clGen.setArg(4, cl_screen(s));
        clGen.setArg(5, fmt);
        queue.enqueueNDRangeKernel(clGen, cl::NullRange, dims, cl::NullRange);
        queue.enqueueReadBuffer(v_dev,CL_TRUE,0,bufferSize,img);
    }
    catch (cl::Error err)
    {
        std::cerr
            << "OpenCL error: "
            << err.what() << "(" << err.err() << ")"
            << std::endl;
        return;
    }
}

FracGen::FracGen(bool bench, bool cpu, std::string vendor)
{
    try
    {
        // Get list of OpenCL platforms.
        std::vector<::cl::Platform> platform;
        cl::Platform::get(&platform);

        if (platform.empty())
        {
            std::cerr << "OpenCL platforms not found." << std::endl;
            exit(1);
        }

        for(auto p = platform.begin(); devices.empty() && p != platform.end(); p++)
        {
            std::vector<cl::Device> pldev;
            try
            {
                if(cpu)
                {
                    p->getDevices(CL_DEVICE_TYPE_CPU, &pldev);
                }
                else
                {
                    p->getDevices(CL_DEVICE_TYPE_GPU, &pldev);
                }

                for(auto d = pldev.begin(); devices.empty() && d != pldev.end(); d++)
                {
                    if (!d->getInfo<CL_DEVICE_AVAILABLE>()) continue;


                    std::string ext = d->getInfo<CL_DEVICE_EXTENSIONS>();
                    std::string devvendor = d->getInfo<CL_DEVICE_VENDOR>();
                    if(vendor != "" && devvendor.find(vendor) == std::string::npos)
                    {
                        continue;
                    }

                    /** If you use double precision on your kernel, you need to
                      * check for this. I initially did so but reverted back to
                      * float because my Titan X was, in some instances, actually
                      * struggling to keep up with my 1920X. Most consumer GPUs,
                      * such as Radeons and GeForce are really bad with doubles.
                      * The Radeon VII is a notable exception. I though the Titans
                      * would also be exceptions but I was wrong. If you' want to
                      * run this on a FirePro, a Quadro, a Radeon Instinct or whatnot,
                      * though it could be interesting to play with this, though you'll
                      * also need to tweak some other things
                      */

//                    if ( (ext.find("cl_khr_fp64") == std::string::npos &&
//                         ext.find("cl_amd_fp64") == std::string::npos))
//                    {
//                        continue;
//                    }
                    devices.push_back(*d);
                    context = cl::Context(devices);
                 }
            }
            catch(...)
            {
                devices.clear();
            }
        }
    }
    catch (const cl::Error &err)
    {
           std::cerr
               << "OpenCL error: "
               << err.what() << "(" << err.err() << ")"
               << std::endl;
           return;
    }
    if (devices.empty())
    {
        if(vendor != "")
        {
            std::cout << "Specifically requested vendor = " << vendor << std::endl;
        }
        //A likely cause if you're using doubles
        //std::cerr << "Devices with double precision not found." << std::endl;
        exit(1);
    }
    static bool once = false;
    if(!once || !bench )
    {
        once = true;
        if(vendor != "")
        {
            std::cout << "Specifically requested vendor = " << vendor << std::endl;
        }
        std::cout << "Running OpenCL on: " << devices[0].getInfo<CL_DEVICE_NAME>()   << std::endl;
        std::cout << "With vendor: "       << devices[0].getInfo<CL_DEVICE_VENDOR>() << std::endl;
        std::cout << "Driver version: "    << devices[0].getInfo<CL_DEVICE_VERSION>() << std::endl;
        std::cout << "OpenCL C version: "  << devices[0].getInfo<CL_DEVICE_OPENCL_C_VERSION>() << std::endl;
    }

    try
    {
        queue = cl::CommandQueue{context, devices[0]};
    }
    catch (const cl::Error &err)
    {
           std::cerr
               << "OpenCL error: "
               << err.what() << "(" << err.err() << ")"
               << std::endl;
           exit(1);
    }
    compileOpenCLKernel();

}

FracGen::~FracGen()
{}
