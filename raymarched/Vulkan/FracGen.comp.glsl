#version 450
#extension GL_ARB_separate_shader_objects : enable

#define WORKGROUP_SIZE 16
layout (local_size_x = WORKGROUP_SIZE, local_size_y = WORKGROUP_SIZE, local_size_z = 1 ) in;

/******************************************************************************
 *
 * Tweakable parameters
 *
 ******************************************************************************/

const uint maxRaySteps       = 7500;
const float collisionMinDist = 0.00055f;

// coulouring parameters

const float hueFactor   = -20.0f;
const int hueOffset     = 10;
const float valueFactor = 32;
const float valueClamp  = 0.8f;
const float satValue    = 0.8f;
const uint bgValue      = 40;


// Mandelbox constants
const float fixedRadiusSq =  2.2f;
const float minRadiusSq   =  0.8f;
const float foldingLimit  =  1.45f;
const float boxScale      = -3.5f;
const uint boxIterations  =  30;


struct PixelFormat
{
    uint Amask;
    // GLSL doesn't have a char or uint8_t type
    uint Rloss;
    uint Gloss;
    uint Bloss;
    uint Aloss;
    uint Rshift;
    uint Gshift;
    uint Bshift;
    uint Ashift;
};

struct Camera
{
    vec3 pos;
    vec3 up;
    vec3 target;
    float AR;
    float near;
    float fovY;
};

struct Screen
{
    vec3 topLeft;
    int width;
    int height;
    float pixelWidth;
    float pixelHeight;
};

layout(binding = 0) buffer buf
{
   uint data[];
};

layout(binding = 1) buffer cam
{
   Camera c;
};

layout(binding = 2) buffer scr
{
   Screen s;
};

layout(binding = 3) buffer fmt
{
   PixelFormat f;
};


/******************************************************************************
 *
 * Distance estimator functions
 *
 ******************************************************************************/


void sphereFold(inout vec3 z, inout float dz)
{
    float rsq = dot(z,z);
    /** Unlike CUDA and HIP, in Vulkan doing the
      * conditional shenanigans does not increase
      * the function's runtime. However, it doesn't
      * seem to decrease it either so there's not
      * really any point in doing it as all you're
      * left with is less readable code
      */
//    int cond1 = int(rsq < minRadiusSq);
//    int cond2 = int((rsq < fixedRadiusSq) && !bool(cond1));
//    int cond3 = int(!bool(cond1) && !bool(cond2));
//    float temp = ( (fixedRadiusSq/minRadiusSq) * cond1) + ( (fixedRadiusSq/rsq) * cond2) + cond3;
//    z *= temp;
//    dz *= temp;

    if ( rsq < minRadiusSq)
    {
        // linear inner scaling
        float temp = (fixedRadiusSq/minRadiusSq);
        z *= temp;
        dz *= temp;
    }
    else if(rsq < fixedRadiusSq )
    {
        // this is the actual sphere inversion
        float temp = (fixedRadiusSq/rsq);
        z *= temp;
        dz *= temp;
    }
}

void boxFold(inout vec3 z)
{
    z = clamp(z, -foldingLimit, foldingLimit)* 2.0f - z;
}

float boxDist(in vec3 p)
{
    /**
     * Distance estimator for a mandelbox
     *
     * Distance estimator adapted from
     * https://http://blog.hvidtfeldts.net/index.php/2011/11/distance-estimated-3d-fractals-vi-the-mandelbox/
     */
    vec3 offset = p;
    float dr = boxScale;
    vec3 z = p;
    for (uint n = 0; n < boxIterations; n++)
    {
        boxFold(z);       // Reflect
        sphereFold(z,dr);    // Sphere Inversion

        z = z * boxScale + offset;  // Scale & Translate
        dr = dr * abs(boxScale) + 1.0f;
    }
    float r = length(z);
    return r/abs(dr);
}


/******************************************************************************
 *
 * Colouring functions
 *
 ******************************************************************************/



uvec4 HSVtoRGB(in int H, in float S, in float V)
{

    /**
     * adapted from
     * https://gist.github.com/kuathadianto/200148f53616cbd226d993b400214a7f
     */

    uvec4 outcolour;
    float C = S * V;
    float X = C * (1 - abs(mod(H / 60.0f, 2) - 1));
    float m = V - C;
    float Rs, Gs, Bs;

    if(H >= 0 && H < 60)
    {
        Rs = C;
        Gs = X;
        Bs = 0;
    }
    else if(H >= 60 && H < 120)
    {
        Rs = X;
        Gs = C;
        Bs = 0;
    }
    else if(H >= 120 && H < 180)
    {
        Rs = 0;
        Gs = C;
        Bs = X;
    }
    else if(H >= 180 && H < 240)
    {
        Rs = 0;
        Gs = X;
        Bs = C;
    }
    else if(H >= 240 && H < 300)
    {
        Rs = X;
        Gs = 0;
        Bs = C;
    }
    else {
        Rs = C;
        Gs = 0;
        Bs = X;
    }

    outcolour.x = uint((Rs + m) * 255) ;
    outcolour.y = uint((Gs + m) * 255) ;
    outcolour.z = uint((Bs + m) * 255) ;
    outcolour.w = 255;

    return outcolour;
}

uvec4 getColour(vec4 steps)
{
    uvec4 colour;

    vec3 position = steps.xyz;

    uvec4 background = uvec4(bgValue.xxx,255);

    if(steps.w == maxRaySteps)
    {
        return background;
    }
    else
    {

        float saturation = satValue;
        int hue = (int(position.z * hueFactor) + hueOffset) % 360;
        float value = 1.0 - min( float( (steps.w*valueFactor) /maxRaySteps), valueClamp);

        colour = HSVtoRGB(hue, saturation, value);

//        colour.r = uint(255*value);
//        colour.g = uint(255*value);
//        colour.b = uint(255*value);

        return colour;
    }
}

uint MapSDLRGBA(uvec4 colour,  PixelFormat format)
{
    return    (colour.r >> format.Rloss) << format.Rshift
            | (colour.g >> format.Gloss) << format.Gshift
            | (colour.b >> format.Bloss) << format.Bshift
            | ((colour.a >> format.Aloss) << format.Ashift & format.Amask  );
}


/******************************************************************************
 *
 * Ray marching function and entry kernel
 *
 ******************************************************************************/

vec4 trace(uint x, uint y)
{
    /**
     * This function taken from
     * http://blog.hvidtfeldts.net/index.php/2011/06/distance-estimated-3d-fractals-part-i/
     */

    float totalDistance = 0.0;
    uint steps;

    vec3 pixelPosition = s.topLeft + vec3(s.pixelWidth*x, s.pixelHeight * y, 0);

    vec3 rayDir = pixelPosition - c.pos;
    rayDir = normalize(rayDir);

    vec3 p;
    for (steps=0; steps < maxRaySteps; steps++)
    {
        p = c.pos + (rayDir * totalDistance);
        float distance = boxDist(p);
        totalDistance += distance;
        if (distance < collisionMinDist) break;
    }
    //return both the steps and the actual position in space for colouring purposes
    return vec4(p, steps);
}


void main()
{

  /*
  In order to fit the work into workgroups, some unnecessary threads are launched.
  We terminate those threads here.
  */
    if(gl_GlobalInvocationID.x >= s.width || gl_GlobalInvocationID.y >= s.height)
    {
        return;
    }

    uint col = gl_GlobalInvocationID.x;
    uint row = gl_GlobalInvocationID.y;
    uint index = ((row*s.width)+col);

    data[index] = MapSDLRGBA(getColour(trace(col, row)), f);
}
