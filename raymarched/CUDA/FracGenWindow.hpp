#ifndef TOYBROT_FRACGENWINDOW_DEFINED
#define TOYBROT_FRACGENWINDOW_DEFINED

#ifdef WIN32
    #include <Windows.h>
    #include "SDL.h"
    #include "SDL_opengl.h"
#else

#include "SDL2/SDL.h"
#include "SDL2/SDL_opengl.h"

#endif

#include <memory>

#include "Vec.cuh"


//Defining some convenience types

using SurfPtr = std::shared_ptr<SDL_Surface>;
using SurfUnq = std::unique_ptr<SDL_Surface>;

struct RGBA{uint8_t r = 0,g = 0,b = 0,a = 255;
            operator uint32_t() const;};

struct Camera{ Vec3f pos, up, target; float AR, near, fovY;};
struct Screen{ Vec3f topLeft; int width, height; float pixelWidth, pixelHeight;};


class FracGenWindow
{
public:
    FracGenWindow(int width, int height, std::string &flavourDesc, std::shared_ptr<bool> redrawFlag, std::shared_ptr<bool> exit);
    ~FracGenWindow();

    void draw(SurfPtr surface);
    void paint();
    double AspectRatio() const noexcept {return cam.AR;}
    bool captureEvents();
    void registerRedrawFlag(std::shared_ptr<bool> b) noexcept {redrawRequired = b;}
    void registerColourFlag(std::shared_ptr<int> i) noexcept {colourScheme = i;}
    SurfPtr getFrac() noexcept {return frac;}
    void setTitle(std::string title);
    void setCamPos(Vec3f p) noexcept {cam.pos = p;}
    void setCamTarget(Vec3f t) noexcept {cam.target = t;}
    Vec3f CamPos() const noexcept {return cam.pos;}
    Vec3f CamTarget() const noexcept {return cam.target;}
    Camera Camera() const noexcept { return cam;}


private:

    void drawHighlight();

    bool onKeyboardEvent(const SDL_KeyboardEvent& e) noexcept;
    bool onMouseMotionEvent(const SDL_MouseMotionEvent& e) noexcept;
    bool onMouseButtonEvent(const SDL_MouseButtonEvent& e ) noexcept;

    void resetHL(int x0, int y0);

    int         width;
    int         height;
    int         colourDepth;
    bool        drawRect;
    SDL_Rect    HL;
    int         mouseX;
    int         mouseY;


    SDL_Window* mainwindow;
    SDL_Renderer* render;
    SurfPtr screen;
    SDL_Texture* texture;
    SurfPtr frac;
    SurfUnq highlight;
    struct Camera cam;


    std::string flavour;
    std::shared_ptr<bool> redrawRequired;
    std::shared_ptr<int> colourScheme;
    std::shared_ptr<bool> exitNow;
};

#endif //TOYBROT_FRACGENWINDOW_DEFINED
